# anonymousBASHconfig
# Maintainer: ryan <ryan -at- mathcrimes.net>
# etc etc etc
# Version: 0.1.3


if [[ $- != *i* ]]; then
	# Shell is non-interactive.  Be done now!
    return
fi

if ((BASH_VERSINFO[0] < 4))
then 
  echo "ERROR: bash-4.0 or greater is required for bashrc + friends." 
fi

# If missing, recreate a new empty history file so apps don't show errors
if [[ -z ${HISTFILE+x} ]]; then
	[[ ! -f "$HOME/.bash_history" ]] && touch "$HOME/.bash_history"
else
	[[ ! -f "$HISTFILE" ]] && touch "$HISTFILE"
fi

# Source global definitions
if [ -f /etc/bashrc ]; then
	. /etc/bashrc
fi

if [[ $EUID -ne 0 ]]; then
	# Grant full access to the user, block write for group, and
	# block read write for others. Leave execute for directories.
	# (Also see fixuserhome function)
	umask u=rwx,g=rx,o=x # umask 026
fi

if [[ -x "$(command -v locate)" ]]; then
	# Case insensitive search and display only files present in your system
	alias locate='locate -i -e'

	# Update the locate database before locating a file
	alias ulocate='sudo updatedb && locate'

	# Always update the locate (mlocate) database as root
	alias updatedb='sudo updatedb'

	# Display the number of matching entries
	alias locount='locate -c'
fi


## GENERAL OPTIONS ##

set -eo pipefail

set -o noclobber				# Use `>|` to force redirection to an existing file
shopt -s checkwinsize			# Update window size after every command

shopt -s no_empty_cmd_completion
shopt -s histappend
shopt -s histverify
shopt -s cmdhist

set -o ignoreeof

shopt -s autocd
shopt -s cdspell
shopt -s dirspell
shopt -s checkjobs
shopt -s cdable_vars

shopt -s hostcomplete
shopt -s globstar
shopt -s extglob
shopt -s nocaseglob;			# Case-insensitive globbing
shopt -s globstar 2> /dev/null	# Recursive globbing (** recurses all dirs)

bind Space:magic-space						# History expansion w/space e.g. !!<space>
bind "set completion-ignore-case on"		# Ignores case w/file completion
bind "set completion-map-case on"			# Hyphens and underscores are same
bind "set show-all-if-ambiguous on"			# First tab shows ambig matches
bind "set mark-symlinked-directories on"	# Add trailing slash for matched symlinks
bind 'set show-all-if-unmodified on'
bind 'set mark-directories on'
bind 'set expand-tilde on'
bind 'set colored-stats on'
bind 'set visible-stats on'

CDPATH="."
PROMPT_COMMAND='history -a'		# Record each line as its issued
HISTSIZE=50000
HISTFILESIZE=100000
HISTCONTROL="erasedups:ignoreboth"
HISTTIMEFORMAT='%F %T '			# Standard ISO 8601 timestamp
PROMPT_DIRTRIM=2 				# Trim long paths in prompt (Bash 4.x)
export HISTIGNORE="&:[ ]*:exit:ls:bg:fg:history:clear:reset:cls:cd"

set -o notify
stty -ctlecho

# Enable incremental history search with up/down arrows (also Readline goodness)
# Learn more about this here: http://codeinthehole.com/writing/the-most-important-command-line-tip-incremental-history-searching-with-inputrc/
bind '"\e[A": history-search-backward'
bind '"\e[B": history-search-forward'
bind '"\e[C": forward-char'
bind '"\e[D": backward-char'

export EDITOR=vim
export PAGER=less
export TERM=xterm-256color

# Set up environment's PATH 
if ! [[ "$PATH" =~ "$HOME/.local/bin:$HOME/bin:" ]]
then
    PATH="$HOME/.local/bin:$HOME/bin:$PATH"
fi
export PATH

if [ -f /usr/local/share/bash-completion/bash_completion ]
then
    . /usr/local/share/bash-completion/bash_completion
fi

# Change the window title of X terminals 
case ${TERM} in
	[aEkx]term*|rxvt*|gnome*|konsole*|interix|tmux*)
		PS1='\[\033]0;\u@\h:\w\007\]'
		;;
	screen*)
		PS1='\[\033k\u@\h:\w\033\\\]'
		;;
	*)
		unset PS1
		;;
esac

lowercase(){
    echo "$1" | sed "y/ABCDEFGHIJKLMNOPQRSTUVWXYZ/abcdefghijklmnopqrstuvwxyz/"
}

OS=$(lowercase `uname`)
OS_TYPE=''
KERNEL=$(uname -r)
MACH=$(uname -m)
DISTRO=''

export OS; export OS_TYPE; export KERNEL; export MACH; export DISTRO;

case $OS in
	windows*)
		export OS_TYPE=windows;;
	linux*)
		export OS_TYPE=linux;;
	*bsd*)
		if [[ "$OS" =~ ^freebsd ]]; then
			export OS_TYPE=freebsd
		else
			export OS_TYPE=bsd
		fi
		;;
	darwin*)
		export OS_TYPE=darwin;;
	cygwin*)
		export OS_TYPE=cygwin;;
	solaris*)
		export OS_TYPE=solaris;;
	msys*)
		export OS_TYPE=windows;;
	*)
		export OS_TYPE=unknown;;
esac
		
# Set colorful PS1 only on colorful terminals.
# dircolors --print-database uses its own built-in database
# instead of using /etc/DIR_COLORS.  Try to use the external file
# first to take advantage of user additions.
# We run dircolors directly due to its changes in file syntax and
# terminal name patching.
use_color=false

if type -P dircolors >/dev/null ; then
	# Enable colors for ls, etc.  Prefer ~/.dir_colors #64489
	LS_COLORS=

	if [[ -f ~/.dir_colors ]] ; then
		eval "$(dircolors -b ~/.dir_colors)"
	elif [[ -f /etc/DIR_COLORS ]] ; then
		eval "$(dircolors -b /etc/DIR_COLORS)"
	else
		eval "$(dircolors -b)"
	fi

	# Note: We always evaluate the LS_COLORS setting even when it's the
	# default.  If it isn't set, then `ls` will only colorize by default
	# based on file attributes and ignore extensions (even the compiled
	# in defaults of dircolors). #583814
	if [[ -n ${LS_COLORS:+set} ]] ; then
		use_color=true
	else
		# Delete it if it's empty as it's useless in that case.
		unset LS_COLORS
	fi
else
	# Some systems (e.g. BSD & embedded) don't typically come with
	# dircolors so we need to hardcode some terminals in here.
	case ${TERM} in
	[aEkx]term*|rxvt*|gnome*|konsole*|screen|tmux|cons25|*color) use_color=true;;
	esac
fi

if ${use_color} ; then
	if [[ ${EUID} == 0 ]] ; then
		PS1+='\[\033[01;31m\]\h\[\033[01;34m\] \w \$\[\033[00m\] '
	else
		PS1+='\[\033[01;32m\]\u@\h\[\033[01;34m\] \w \$\[\033[00m\] '
	fi
else
	# show root@ when we don't have colors
	PS1+='\u@\h \w \$ '
fi

# Uncomment the following line if you don't like systemctl's auto-paging feature:
# export SYSTEMD_PAGER=

# quick ssh hostname completion
[ -e "$HOME/.ssh/config" ] && complete -o "default" -o "nospace" -W "$(grep "^Host" ~/.ssh/config | grep -v "[?*]" | cut -d " " -f2)" scp sftp ssh

# User specific aliases and functions
#if [ -d ~/.bashrc.d ]; then
#	for rc in ~/.bashrc.d/*; do
#		if [ -f "$rc" ]; then
#			. "$rc"
#		fi
#	done
#fi

if [[ -d "$HOME/.bashrc.d" ]]; then
	if [[ -e "$HOME/.bashrc.d/bash_ssh" ]]; then
		. "$HOME/.bashrc.d/bash_ssh"
	fi

	if [[ -e "$HOME/.bashrc.d/bash_aliases" ]]; then
		. "$HOME/.bashrc.d/bash_aliases"
	fi

	if [[ -e "$HOME/.bashrc.d/bash_functions" ]]; then
		. "$HOME/.bashrc.d/bash_functions"
	fi

	if [[ -e "$HOME/.bashrc.d/bash_colors" ]]; then
		. "$HOME/.bashrc.d/bash_colors"
	fi
fi

unset rc

if [ -f "$HOME/.cargo/env" ]; then
	. "$HOME/.cargo/env"
fi

# Colors for less
export LESS_TERMCAP_mb=$'\e[1;32m'
export LESS_TERMCAP_md=$'\e[1;32m'
export LESS_TERMCAP_me=$'\e[0m'
export LESS_TERMCAP_se=$'\e[0m'
export LESS_TERMCAP_so=$'\e[01;33m'
export LESS_TERMCAP_ue=$'\e[0m'
export LESS_TERMCAP_us=$'\e[1;4;31m'

# Prompt
RESET="\e[0m"
GREEN="\e[32m"
YELLOW="\e[93m"
RED="\e[91m"
BLUE="\e[34m"

export RESET; export GREEN; export YELLOW; export RED; export BLUE;
export GCC_COLORS='error=01;31:warning=01;35:note=01;36:caret=01;32:locus=01:quote=01'

# make less more friendly for non-text input files, see lesspipe(1)
[ -x /usr/bin/lesspipe ] && eval "$(SHELL=/bin/sh lesspipe)"

# Try to keep environment pollution down, EPA loves us.
unset use_color sh

# BEGIN_KITTY_SHELL_INTEGRATION
# if test -e "$HOME/src/kitty/shell-integration/kitty.bash"; then source "$HOME/src/kitty/shell-integration/kitty.bash"; fi
# END_KITTY_SHELL_INTEGRATION

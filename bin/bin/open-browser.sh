# 
# open text browser
#

declare -a browsers=(browsh w3m links lynx elinks links2)

for i in "${!browsers[@]}"; do

	if command -v ${browsers[$i]}; then
		echo ${browsers[$i]}
	fi
done

#if command -v browsh; then
